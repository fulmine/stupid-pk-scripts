# Shitty PK Scripts

A very bad CLI tool for running bulk system management operations using [PluralKit](https://pluralkit.me/)'s api. I am not liable for any lost information.

Currently includes things such as...
1. bulk group subcategory privacy (bulk-sets the visibility of groups starting with a given prefix)
2. Adding all members to a single group
3. Copying a group's member list to another group
4. Copying a member's group list to another member
5. Listing all member names from a specific group, wrapping member names in quotes if they contain spaces
6. Listing all member names from the system as a whole, same as above
7. Listing all group names from the system as a whole, same as above

## Running
1. install [Deno](https://deno.land/) first!
2. `git clone https://codeberg.org/fulmine/stupid-pk-scripts.git`
3. put a `.env` at the root of the repo containing the following (or alternatively just set an environment variable containing the following)
  - `TOKEN`: your pluralkit token
4. run `deno run -A main.ts`
5. Enjoy????

## License
[WTFPL](./LICENSE)