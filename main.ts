import { load } from "https://deno.land/std@0.188.0/dotenv/mod.ts";
const env = await load()
const token = env.TOKEN ?? Deno.env.get("TOKEN")?.toString()

if (!token) {
    console.error("Please put your token in an .env file as TOKEN=\"tokenhere\"")
    Deno.exit(1)
}

console.log("\nHi! Which one of these scripts do you want to run?\n")

const scripts = [
    {
        func: async () => await hitDaBricks(),
        name: "Exit program",
        style: "color: red",
    },
    {
        func: async () => await bulkGroupPrivacy(),
        name: "Bulk group subcategory privacy",
        style: "",
    },
    {
        func: async () => await addAllMembersToGroup(),
        name: "Add all members to a single group",
        style: ""
    },
    {
        func: async () => await copyGroupMembers(),
        name: "Copy group member list to another group",
        style: ""
    },
    {
        func: async () => await copyMemberGroups(),
        name: "Copy group list from one member to another",
        style: ""
    },
    {
        func: async () => await listGroupMemberNames(),
        name: "List all member names from a specific group",
        style: ""
    },
    {
        func: async() => await listAllMemberNames(),
        name: "List all member names in the entire system",
        style: ""
    },
    {
        func: async() => await listAllGroupNames(),
        name: "List all group names in the entire system",
        style: ""
    },
    {
        func: async () => await renameGroupPrefix(),
        name: "Rename a collection of groups by prefix",
        style: ""
    }
]

const index = promptForNumber()
await scripts[index].func()

function promptForNumber(): number {
    scripts.forEach((script, index) => console.log(`%c${index}. %c${script.name}`, "color: gray", script.style))

    const input = prompt("\nEnter any of the following numbers to continue.\n>")
    const number = parseInt(input || "")

    if (isNaN(number)) {
        console.log("%cNot a valid number, %cplease try again", "color: red")
        return promptForNumber()
    }

    if (number < 0 || number > scripts.length - 1) {
        console.log("%cThis script doesn't exist! %cplease try again", "color: red")
        return promptForNumber()
    }

    return number
}

async function bulkGroupPrivacy() {
    console.log("%cExecuting bulk group privacy!", "color: green")
    const searchfor = prompt("What is the prefix for the groups you're editing privacy for?\n>")
    if (!searchfor) {
        console.log("%cNo search input provided, exiting", "color: red")
        return
    }

    const input = prompt("Public or private? (default: private)\n>")
    let privacy: "public"|"private" = "private"
    if (!input) privacy = "private"
    else if (["private", "priv", "false", "hidden", "invisible"].some(p => input === p)) privacy = "private"
    else if (["public", "pub", "true", "visible", "show"].some(p => input === p)) privacy = "public"
    else {
        console.log("%cWell that's a privacy level I haven't heard of, exiting", "color: red")
        return
    }

    try { 
        const resp = await fetch(`https://api.pluralkit.me/v2/systems/@me/groups`, {
            headers: {
                "Authorization": token
            }
        })
        const groups: any[] = await resp.json()

        const relevantGroups: any[] = groups.filter(g => g.name.startsWith(searchfor))

        for (const [i, g] of relevantGroups.entries()) {
            console.log(`%c[${i + 1}/${relevantGroups.length}]%c Updating privacy of %c${g.name}%c...`, "color: gray", "", "color: cyan", "")
            await fetch(`https://api.pluralkit.me/v2/groups/${g.uuid}`, {
                method: "PATCH",
                headers: {
                    "Authorization": token,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    privacy: {
                        visibility: privacy
                    }
                })
            })
            await new Promise((resolve) => setTimeout(resolve, 1000))
        }
        
        console.log(`Done! %c(Updated ${relevantGroups.length} groups)`, "color: gray")
    } catch (error) {
        console.log(`%c${error.message}`, "color: red")
    }

    console.log("%cExiting...", "color: gray")
}

async function addAllMembersToGroup() {
    console.log("%cExecuting adding all members to a single group!", "color: green")
    
    const gid = prompt("What is the group id?\n>")
    if (!gid) {
        console.log("%cNo group id provided, exiting", "color: red")
        return
    }

    try {
        const resp = await fetch(`https://api.pluralkit.me/v2/systems/@me/members`, {
            headers: {
                "Authorization": token
            }
        })
        const members = await resp.json()
        const mids = members.map((m: any) => m.uuid)

        await fetch(`https://api.pluralkit.me/v2/groups/${gid}/members/overwrite`, {
            method: "POST",
            headers: {
                "Authorization": token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(mids)
        })

        console.log(`Done! %c(Added all ${mids.length} members)`, "color: gray")
    } catch (error) {
        console.log(`%c${error.message}`, "color: red")
    }

    console.log("%cExiting...", "color: gray")
}

async function copyGroupMembers() {
    console.log("%cExecuting copy group member list to another group!", "color: green")

    const ogid = prompt("What is the original group id?\n>")
    if (!ogid) {
        console.log("%cNo group id provided, exiting", "color: red")
        return
    }

    const ngid = prompt("What is the group id I need to copy the member list to?\n>")
    if (!ngid) {
        console.log("%cNo group id provided, exiting", "color: red")
        return
    }

    try {
        const resp = await fetch(`https://api.pluralkit.me/v2/groups/${ogid}/members`, {
            headers: {
                "Authorization": token
            }
        })
        const members = await resp.json()
        const mids = members.map((m: any) => m.uuid)

        await fetch(`https://api.pluralkit.me/v2/groups/${ngid}/members/overwrite`, {
            method: "POST",
            headers: {
                "Authorization": token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(mids)
        })

        console.log(`Done! %c(Copied all ${mids.length} members)`, "color: gray")
    } catch (error) {
        console.log(`%c${error.message}`, "color: red")
    }

    console.log("%cExiting...", "color: gray")
}

async function copyMemberGroups() {
    console.log("%cExecuting copy member group list to another member!", "color: green")

    const omid = prompt("What is the original member id?\n>")
    if (!omid) {
        console.log("%cNo member id provided, exiting", "color: red")
        return
    }

    const nmid = prompt("What is the member id I need to copy the groups to?\n>")
    if (!nmid) {
        console.log("%cNo member id provided, exiting", "color: red")
        return
    }

    try {
        const resp = await fetch(`https://api.pluralkit.me/v2/members/${omid}/groups`, {
            headers: {
                "Authorization": token
            }
        })
        const groups = await resp.json()
        const gids = groups.map((g: any) => g.uuid)

        await fetch(`https://api.pluralkit.me/v2/members/${nmid}/groups/overwrite`, {
            method: "POST",
            headers: {
                "Authorization": token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(gids)
        })

        console.log(`Done! %c(Copied all ${gids.length} groups)`, "color: gray")
    } catch (error) {
        console.log(`%c${error.message}`, "color: red")
    }

    console.log("%cExiting...", "color: gray")
}

async function listGroupMemberNames() {
    const gid = prompt("I'll need your group id\n>")
    
    try {
        const resp = await fetch(`https://api.pluralkit.me/v2/groups/${gid}/members`, {
        headers: {
            "Authorization": token
        }
        })
        const members = await resp.json()
        const names = members.map((m: { name: string }) => {
            if (m.name.includes(" ")) return `"${m.name}"`
            else return m.name
        })

        console.log(names.join(" "))
    } catch (error) {
        console.error(error)
        return
    }
}
async function listAllMemberNames() {
    try {
        const resp = await fetch(`https://api.pluralkit.me/v2/systems/@me/members`, {
        headers: {
            "Authorization": token
        }
        })
        const members = await resp.json()
        const names = members.map((m: { name: string }) => {
            if (m.name.includes(" ")) return `"${m.name}"`
            else return m.name
        })

        console.log(names.join(" "))
    } catch (error) {
        console.error(error)
        return
    }
}

async function listAllGroupNames() {
    try {
        const resp = await fetch(`https://api.pluralkit.me/v2/systems/@me/groups`, {
        headers: {
            "Authorization": token
        }
        })
        const members = await resp.json()
        const names = members.map((m: { name: string }) => {
            if (m.name.includes(" ")) return `"${m.name}"`
            else return m.name
        })

        console.log(names.join(" "))
    } catch (error) {
        console.error(error)
        return
    }
}

async function renameGroupPrefix() {
    console.log("%cExecuting bulk group rename!", "color: green")
    const searchfor = prompt("What is the prefix you're renaming?\n>")
    if (!searchfor) {
        console.log("%cNo search input provided, exiting", "color: red")
        return
    }

    const input = prompt("What are we renaming it to?")

    try { 
        const resp = await fetch(`https://api.pluralkit.me/v2/systems/@me/groups`, {
            headers: {
                "Authorization": token
            }
        })
        const groups: any[] = await resp.json()

        const relevantGroups: any[] = groups.filter(g => g.name.startsWith(searchfor))

        for (const [i, g] of relevantGroups.entries()) {
            console.log(`%c[${i + 1}/${relevantGroups.length}]%c Updating name for %c${g.name}%c...`, "color: gray", "", "color: cyan", "")
            await fetch(`https://api.pluralkit.me/v2/groups/${g.uuid}`, {
                method: "PATCH",
                headers: {
                    "Authorization": token,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    name: g.name.replace(searchfor, input)
                })
            })
            await new Promise((resolve) => setTimeout(resolve, 500))
        }
        
        console.log(`Done! %c(Updated ${relevantGroups.length} groups)`, "color: gray")
    } catch (error) {
        console.log(`%c${error.message}`, "color: red")
    }

    console.log("%cExiting...", "color: gray")
}

function hitDaBricks() {
    console.log("Ok bye")
}